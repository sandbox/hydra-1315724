<?php

function views_skip_views_data_alter(&$data) {
  foreach ($data as $field_title => $field_content) {
    $subfield = substr($field_title, 0, 11);
    // referencefields are making trouble
    if ($subfield == 'field_data_' && !strstr($field_title, 'reference')) {
      $identifer = str_replace($subfield, '', $field_title);
      $handler = $field_content[$identifer]['field']['handler'];
      if ($handler == 'views_handler_field_field') {
        $data[$field_title][$identifer]['field']['handler'] = 'views_handler_field_field_extend_skip';
      }
    }
  }
//  title currently not supported
//  $data['node']['title']['field']['handler'] = 'views_handler_field_field_extend_skip';
}
