<?php

class views_handler_field_field_extend_skip extends views_handler_field_field {

  /*
   * Extends the rewrite options with the avalible skip options
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['alter']['contains']['skip'] = array('default' => FALSE);
    $options['alter']['contains']['skip_length'] = array('default' => '');
    $options['alter']['contains']['skip_word_boundary'] = array('default' => TRUE);
    $options['alter']['contains']['skip_ellipsis'] = array('default' => TRUE);
    $options['alter']['contains']['skip_html'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['alter']['skip'] = array(
        '#type' => 'checkbox',
        '#title' => t('Skip the first x characters'),
        '#description' => t('Enable to skip a defined lenght of characters'),
        '#default_value' => $this->options['alter']['skip'],
    );

    $form['alter']['skip_length'] = array(
        '#title' => t('Skip length'),
        '#type' => 'textfield',
        '#default_value' => $this->options['alter']['skip_length'],
        '#description' => t('The numer of characters to skip'),
        '#dependency' => array(
            'edit-options-alter-skip' => array(1),
        ),
    );
    $form['alter']['skip_word_boundary'] = array(
        '#type' => 'checkbox',
        '#title' => t('Trim only on a word boundary'),
        '#description' => t('If checked, this field be trimmed only on a word boundary. This is guaranteed to be the maximum characters stated or less. If there are no word boundaries this could trim a field to nothing.'),
        '#default_value' => $this->options['alter']['skip_word_boundary'],
        '#dependency' => array(
            'edit-options-alter-skip' => array(1),
        ),
    );
    $form['alter']['skip_ellipsis'] = array(
        '#type' => 'checkbox',
        '#title' => t('Add an ellipsis'),
        '#description' => t('If checked, a "..." will be added if a field was trimmed.'),
        '#default_value' => $this->options['alter']['skip_ellipsis'],
        '#dependency' => array(
            'edit-options-alter-skip' => array(1),
        ),
    );
    $form['alter']['skip_html'] = array(
        '#type' => 'checkbox',
        '#title' => t('Field can contain HTML'),
        '#description' => t('If checked, HTML corrector will be run to ensure tags are properly closed after trimming.'),
        '#default_value' => $this->options['alter']['skip_html'],
        '#dependency' => array(
            'edit-options-alter-skip' => array(1),
        ),
    );
  }

  /**
   * Perform an advanced text render for the item.
   *
   * This is separated out as some fields may render lists, and this allows
   * each item to be handled individually.
   */
  function render_text($alter) {
    $value = parent::render_text($alter);

    if (!empty($alter['skip']) && !empty($alter['skip_length'])) {
      $value = $this->views_skip_text($alter, $value);
    }

    return $value;
  }

  /**
   * Skip the defined length of charactes.
   *
   * @param $alter
   *   - skip_length: The length of characters that has to be skiped.
   *   - skip_word_boundary: Trim only on a word boundary.
   *   - skip_ellipsis: Trim only on a word boundary.
   *   - skip_html: Take sure that the html is correct.
   *
   * @param $value
   *   The string which should be trimmed.
   */
  function views_skip_text($alter, $value) {
    // Check if the value has enoght characters
    if (drupal_strlen($value) > $alter['skip_length']) {
      // If you wish to skip only at the end of a wod boundary, the value will be trimmed
      // by views_trim_text and the result will be ground-off the value
      // If not the length will simply substringed from the value
      if (!empty($alter['skip_word_boundary'])) {
        $alter_trimmed = $alter;
        $alter_trimmed['ellipsis'] = 0;
        $alter_trimmed['trim'] = $alter['skip'];
        $alter_trimmed['max_length'] = $alter['skip_length'];
        $alter_trimmed['word_boundary'] = $alter['skip_word_boundary'];
        $alter_trimmed['html'] = $alter['skip_html'];

        $trimmed = views_trim_text($alter_trimmed, $value);
        $value = str_replace($trimmed, '', $value);
      } else {
        $value = substr($value, $alter['skip_length']); //changed
      }
      // Adds an ellipsis in front of the value
      if (!empty($alter['skip_ellipsis'])) {
        $value = t('...') . $value;
      }
    }

    if (!empty($alter['skip_html'])) {
      $value = _filter_htmlcorrector($value);
    }

    return $value;
  }

}